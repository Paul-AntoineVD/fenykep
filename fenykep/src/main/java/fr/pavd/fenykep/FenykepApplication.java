package fr.pavd.fenykep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FenykepApplication {

	public static void main(String[] args) {
		SpringApplication.run(FenykepApplication.class, args);
	}

}
